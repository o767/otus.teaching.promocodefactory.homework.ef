﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        private PromoCodeContext Context { get; }

        public EfRepository(PromoCodeContext context)
        {
            Context = context;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, object>> include = null)
        {
            var query = Context.Set<T>();

            if (include != null)
                query.Include(include);

            return await query.ToListAsync();
        }

        public async Task<T> GetByIdAsync(Guid id, Expression<Func<T,object>> include = null)
        {
            var query = Context.Set<T>();

            if (include != null)
                query.Include(include);

            return await query.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task AddAsync(T entity)
        {
            await Context.Set<T>().AddAsync(entity);
            await Context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T entity)
        {
            Context.Set<T>().Update(entity);
            await Context.SaveChangesAsync();
        }

        public async Task DeleteAsync(T entity)
        {
            Context.Set<T>().Remove(entity);
            await Context.SaveChangesAsync();
        }
    }
}