﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database
{
    public class PromoCodeContext: DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        public PromoCodeContext(DbContextOptions<PromoCodeContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<PromoCode>().HasOne(x => x.PartnerManager).WithMany();
            modelBuilder.Entity<PromoCode>().HasOne(x => x.Preference).WithMany();
            modelBuilder.Entity<Employee>().HasOne(x => x.Role).WithMany();
            modelBuilder.Entity<Customer>().HasMany(x => x.Promocodes).WithOne().OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<CustomerPreference>().HasKey(x => new { x.CustomerId, x.PreferenceId });
            modelBuilder.Entity<CustomerPreference>().HasOne(x => x.Customer).WithMany(x => x.Preferences).HasForeignKey(x => x.CustomerId);
            modelBuilder.Entity<CustomerPreference>().HasOne(x => x.Preference).WithMany().HasForeignKey(x => x.PreferenceId);

            modelBuilder.Entity<PromoCode>(x =>
            {
                x.Property(x => x.Code).HasMaxLength(1024);
                x.Property(x => x.ServiceInfo).HasMaxLength(1024);
                x.Property(x => x.PartnerName).HasMaxLength(1024);
            });
            modelBuilder.Entity<Employee>(x =>
            {
                x.Property(x => x.FirstName).HasMaxLength(1024);
                x.Property(x => x.LastName).HasMaxLength(1024);
                x.Property(x => x.Email).HasMaxLength(1024);
            });
            modelBuilder.Entity<Role>(x =>
            {
                x.Property(x => x.Name).HasMaxLength(1024);
                x.Property(x => x.Description).HasMaxLength(1024);
            });
            modelBuilder.Entity<Customer>(x =>
            {
                x.Property(x => x.FirstName).HasMaxLength(1024);
                x.Property(x => x.LastName).HasMaxLength(1024);
                x.Property(x => x.Email).HasMaxLength(1024);
            });
            modelBuilder.Entity<Preference>(x =>
            {
                x.Property(x => x.Name).HasMaxLength(1024);
            });

            modelBuilder.Entity<Employee>().Ignore(x => x.FullName);
            modelBuilder.Entity<Customer>().Ignore(x => x.FullName);
        }
    }
}
