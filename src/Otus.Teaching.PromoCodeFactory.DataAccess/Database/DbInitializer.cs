﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Database
{
    public static class DbInitializer
    {
        public static async Task InitializeAsync(PromoCodeContext context)
        {
            await context.Database.MigrateAsync();

            if (context.Employees.Any())
            {
                return;
            }

            await context.Employees.AddRangeAsync(FakeDataFactory.Employees);
            await context.Preferences.AddRangeAsync(FakeDataFactory.Preferences);
            await context.Customers.AddRangeAsync(FakeDataFactory.Customers);
            var roleIds = FakeDataFactory.Employees.Select(x => x.Role.Id).ToList();
            await context.Roles.AddRangeAsync(FakeDataFactory.Roles.Where(x => !roleIds.Contains(x.Id)));
            await context.SaveChangesAsync();
        }
    }
}
