﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController
        : ControllerBase
    {
        private readonly IRepository<Preference> _repositoryPreference;

        public PreferencesController(IRepository<Preference> repositoryPreference)
        {
            _repositoryPreference = repositoryPreference;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<PrefernceResponse>>> GetPreferencesAsync() =>
            (await _repositoryPreference.GetAllAsync()).Select(x => new PrefernceResponse
            {
                Id = x.Id,
                Name = x.Name
            }).ToList();
    }
}