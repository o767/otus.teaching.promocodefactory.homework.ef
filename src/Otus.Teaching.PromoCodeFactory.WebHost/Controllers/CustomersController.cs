﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Database;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _repositoryCustomer;
        private readonly IRepository<Preference> _repositoryPreference;

        public CustomersController(IRepository<Customer> repositoryCustomer, IRepository<Preference> repositoryPreference)
        {
            _repositoryCustomer = repositoryCustomer;
            _repositoryPreference = repositoryPreference;
        }

        /// <summary>
        /// Список клиентов
        /// </summary>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync()
        {
            return (await _repositoryCustomer.GetAllAsync()).Select(x => new CustomerShortResponse
            {
                Id = x.Id,
                FirstName = x.FirstName,
                LastName = x.LastName,
                Email = x.Email
            }).ToList();
        }

        /// <summary>
        /// Клиент по id
        /// </summary>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _repositoryCustomer.GetByIdAsync(id, x => x.Promocodes);
            return new CustomerResponse
            {
                Id = customer.Id,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Email = customer.Email,
                PromoCodes = customer.Promocodes?.Select(x => new PromoCodeShortResponse
                {
                    Id = x.Id,
                    Code = x.Code,
                    ServiceInfo = x.ServiceInfo,
                    BeginDate = x.BeginDate.ToString(CultureInfo.CurrentCulture),
                    EndDate = x.EndDate.ToString(CultureInfo.CurrentCulture),
                    PartnerName = x.PartnerName
                }).ToList()
            };
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var preferences =
                request.PreferenceIds?.Select(async x => await _repositoryPreference.GetByIdAsync(x))
                    .Select(x => x.Result).Where(x => x != null).ToList() ?? new List<Preference>();
            var customer = new Customer
            {
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            await _repositoryCustomer.AddAsync(customer);

            customer.Preferences = request.PreferenceIds?.Select(async x => await _repositoryPreference.GetByIdAsync(x))
                .Select(x => x.Result).Where(x => x != null).Select(x => new CustomerPreference
                {
                    CustomerId = customer.Id,
                    PreferenceId = x.Id,
                }).ToList() ?? new List<CustomerPreference>();

            await _repositoryCustomer.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Изменить клиента
        /// </summary>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _repositoryCustomer.GetByIdAsync(id);

            if (customer == null)
                return NotFound();

            customer.Id = id;
            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;
            customer.Preferences = request.PreferenceIds?.Select(async x => await _repositoryPreference.GetByIdAsync(x))
                .Select(x => x.Result).Where(x => x != null).Select(x => new CustomerPreference
                {
                    CustomerId = id,
                    PreferenceId = x.Id,
                }).ToList();

            await _repositoryCustomer.UpdateAsync(customer);

            return Ok();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var entity = await _repositoryCustomer.GetByIdAsync(id);

            if (entity == null)
                return NotFound();

            await _repositoryCustomer.DeleteAsync(entity);

            return Ok();
        }
    }
}