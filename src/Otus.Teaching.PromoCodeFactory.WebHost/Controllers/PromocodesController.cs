﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _repositoryPromoCode;
        private readonly IRepository<Customer> _repositoryCustomer;
        private readonly IRepository<Preference> _repositoryPreference;

        public PromocodesController(IRepository<PromoCode> repositoryPromoCode, IRepository<Customer> repositoryCustomer, IRepository<Preference> repositoryPreference)
        {
            _repositoryPromoCode = repositoryPromoCode;
            _repositoryCustomer = repositoryCustomer;
            _repositoryPreference = repositoryPreference;
        }
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            return (await _repositoryPromoCode.GetAllAsync()).Select(x => new PromoCodeShortResponse
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString(CultureInfo.CurrentCulture),
                EndDate = x.EndDate.ToString(CultureInfo.CurrentCulture),
                PartnerName = x.PartnerName
            }).ToList();
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = (await _repositoryPreference.GetAllAsync()).FirstOrDefault(x => x.Name == request.Preference);

            if (preference == null)
                return Ok();

            var customers = (await _repositoryCustomer.GetAllAsync(x => x.Preferences)).Where(x =>
                x.Preferences?.Select(x => x.PreferenceId).Contains(preference.Id) ?? false).ToList();

            if (!customers.Any())
                return Ok();

            foreach (var customer in customers)
            {
                var promoCode = new PromoCode
                {
                    Code = request.PromoCode,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Preference = preference
                };
                await _repositoryPromoCode.AddAsync(promoCode);

                if (customer.Promocodes == null)
                    customer.Promocodes = new List<PromoCode>(new PromoCode[] {promoCode});
                else 
                    customer.Promocodes.Add(promoCode) ;

                await _repositoryCustomer.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}